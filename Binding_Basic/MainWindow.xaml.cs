﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Binding_Basic
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        Student stu;
        public MainWindow()
        {
            InitializeComponent();

            stu = new Student();
            Binding binding = new Binding();
            binding.Source = stu;//设置Binding源为stu对象
            binding.Path = new PropertyPath("Name");//绑定对象的目标属性(Path)
            //将Binding绑定到UI界面的txtBox对象的Text属性
            BindingOperations.SetBinding(this.txtBox, TextBox.TextProperty, binding);
            //this.txtBox.SetBinding(TextBox.TextProperty, new Binding("Name") { Source =stu=new Student()});

        }
        private void btnClick_Click(object sender, RoutedEventArgs e)
        {
            stu.Name += "lhl-";//设置Name属性值  
        }
    }

    public class Student : INotifyPropertyChanged
    {
        public string name;
        public event PropertyChangedEventHandler PropertyChanged;
        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged.Invoke(this, new PropertyChangedEventArgs("name"));
                }
            }
        }
    }
}
