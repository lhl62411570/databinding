﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ItemsSource
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            List<Student> stuList = new List<Student>()
            {
                new Student(){ Id=1,Name="Tom1",Age=40},
                new Student(){ Id=2,Name="Tom2",Age=41},
                new Student(){ Id=3,Name="Tom3",Age=42},
                new Student(){ Id=4,Name="Tom4",Age=43}
            };
            this.listBoxStudents.ItemsSource = stuList;
           // this.listBoxStudents.DisplayMemberPath = "Name";
            Binding binding = new Binding("SelectedItem.Id") { Source = this.listBoxStudents };
            this.txtID.SetBinding(TextBox.TextProperty, binding);
        }
    }
    public class Student
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
    }
}
