﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MutliBinding1
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            Binding b1 = new Binding("Text") { Source=this.txt1};
            Binding b2 = new Binding("Text") { Source = this.txt2 };
            Binding b3 = new Binding("Text") { Source = this.txt3 };
            Binding b4 = new Binding("Text") { Source = this.txt4 };

            MultiBinding mb = new MultiBinding() { Mode=BindingMode.OneWay};
            mb.Bindings.Add(b1);
            mb.Bindings.Add(b2);
            mb.Bindings.Add(b3);
            mb.Bindings.Add(b4);
            mb.Converter = new LoginMutiBindingConvert();
            this.btnClick.SetBinding(Button.IsEnabledProperty, mb);
        }

        private void btnClick_Click(object sender, RoutedEventArgs e)
        {

        }
    }

    public class LoginMutiBindingConvert : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values.Cast<string>().Any(t => string.IsNullOrEmpty(t)))
            {
                return false;
            }
            if(values[0].ToString()==values[1].ToString() && values[2].ToString() == values[3].ToString())
            {
                return true;
            }
            return false;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
