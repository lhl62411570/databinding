﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DependencyObj
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        Student stu;
        public MainWindow()
        {
            InitializeComponent();
            //stu = new Student();
            //Binding binding = new Binding("Text") { Source = txt1 };
            //BindingOperations.SetBinding(stu,Student.NameProperty,binding);
            //txt2.SetBinding(TextBox.TextProperty, binding);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //Student stu = new Student();
            //stu.Name = this.txt1.Text;
            //txt2.Text = stu.Name;
            ////stu.SetValue(Student.NameProperty, this.txt1.Text);
            ////txt2.Text = (string)stu.GetValue(Student.NameProperty);
            ////MessageBox.Show(stu.GetValue(Student.NameProperty).ToString());
        }
    }
    public class Student : DependencyObject
    {
        public string Name
        {
            get { return (string)GetValue(NameProperty); }
            set { SetValue(NameProperty, value); }
        }

        public static readonly DependencyProperty NameProperty = DependencyProperty.Register("Name",
            typeof(string), typeof(Student));
    }
}
