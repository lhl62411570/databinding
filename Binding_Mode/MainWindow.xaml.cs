﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Binding_Mode
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            List<string> strList = new List<string>() { "Tim", "Tom", "Blog" };
            this.txt1.SetBinding(TextBox.TextProperty, new Binding("/") { Source = strList });
            this.txt2.SetBinding(TextBox.TextProperty, new Binding("/Length") { Source = strList,Mode=BindingMode.OneWay });
            this.txt3.SetBinding(TextBox.TextProperty, new Binding("/[2]") { Source = strList, Mode = BindingMode.OneWay });
        }
    }
}
